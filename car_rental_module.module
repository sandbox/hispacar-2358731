<?php

/**
 * @file
 * Car Rental Module. Car hire search for drupal.
 */

/**
 * Implements hook_block_help().
 */
function car_rental_module_help($path, $arg) {
  $help_txt = '';

  switch ($path) {
    case "admin/help#car_rental_module":
      $help_txt .= '<p>';
      $help_txt .= t("Search box to compare 750 car rental companies in more than 30.000 destinations worldwide for the cheapest car hire prices");
      $help_txt .= '</p>';
      break;
  }

  return $help_txt;
}


/**
 * Implements hook_block_info().
 */
function car_rental_module_block_info() {
  $blocks['car_rental_module'] = array(
    // The name that will appear in the block list.
    'info' => t('Car rental module'),
    // Default setting.
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  return $blocks;
}


/**
 * Implements hook_block_view().
 *
 * Prepares the contents of the block.
 */
function car_rental_module_block_view($delta = '') {

  // Add the jQuery UI autocomplete library.
  drupal_add_library('system', 'ui.autocomplete');
  drupal_add_library('system', 'ui.datepicker');

  // Scripts load.
  drupal_add_js(drupal_get_path('module', 'car_rental_module') . '/common.js');

  // CSS load.
  drupal_add_css(drupal_get_path('module', 'car_rental_module') . '/car_rental_module.css', array('group' => CSS_DEFAULT, 'type' => 'file'));

  $block = array();

  switch ($delta) {

    case 'car_rental_module':
      $block['subject'] = variable_get('car_rental_module');
      if (user_access('access content')) {
        $block['content'] = car_rental_module_render();
      }
      break;

  }
  return $block;
}


/**
 * Implements hook_menu().
 */
function car_rental_module_menu() {
  $items = array();
  $items['admin/config/content/car_rental_module'] = array(
    'title' => 'Car rental module',
    'description' => 'Configuration for Car rental module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('car_rental_module_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}


/**
 * Page callback: Car rental module settings.
 */
function car_rental_module_form($form, &$form_state) {
  $form['car_rental_module_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('car_rental_module_title', ''),
    '#size' => 40,
    '#maxlength' => 40,
    '#description' => t('Introduce an appropriate title'),
    '#required' => FALSE,
  );

  $form['car_rental_module_af_cod'] = array(
    '#type' => 'textfield',
    '#title' => t('Affiliate ID'),
    '#default_value' => variable_get('car_rental_module_af_cod', ''),
    '#size' => 40,
    '#maxlength' => 40,
    '#description' => t('If you want to monetize the plugin you need to request an affiliate ID. Click on the !link link !link_close to register and request an affiliate ID. The searchbox will work fine for non-registered sites as well, however to get paid you must use an affiliate ID other wise the plugin will be unable to track any commissions.', array('!link' => '<a href="http://www.hispacar.com/drupal-plugins/search/en/register/" target="_blank">', '!link_close' => '</a>')),
    '#required' => FALSE,
  );

  $form['car_rental_module_lang'] = array(
    '#type' => 'select',
    '#title' => t('Search box language'),
    '#options' => array(
      'en' => t('English'),
      'es' => t('Español'),
    ),
    '#default_value' => variable_get('car_rental_module_lang', ''),
    '#description' => t('Select the language you would like to use on the search box. If you do not select a language English will be used by default.'),
  );

  $form['car_rental_module_default_loc_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Default location id (optional)'),
    '#default_value' => variable_get('car_rental_module_default_loc_id', ''),
    '#size' => 40,
    '#maxlength' => 40,
    '#description' => t('This field can be set to a default location if you prefer. Visitors can still search for a car in any destination worldwide by typing in a different destination. !link Help me find the id !link_close', array('!link' => '<a href="#" onclick="javascript:window.open(\'http://www.hispacar.com/affiliate/id_selector/id_selector.php?lang=en\', \'\', \'width=520, height=320\')">', '!link_close' => '</a>')),
    '#required' => FALSE,
  );

  $form['car_rental_module_format'] = array(
    '#type' => 'select',
    '#title' => t('Format'),
    '#options' => array(
      'c_c_c_half_width' => t('1 column'),
      'c_c_c_full_width' => t('2 columns'),
    ),
    '#default_value' => variable_get('car_rental_module_format', ''),
    '#description' => t('Define the number of columns of the search box (1 or 2)'),
  );

  $form['car_rental_module_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width (%)'),
    '#default_value' => variable_get('car_rental_module_width', 100),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Define the width of the search box. The value is set to 100% by default'),
    '#required' => FALSE,
  );

  $form['car_rental_module_foreground_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Foreground color (#)'),
    '#default_value' => variable_get('car_rental_module_foreground_color', '000000'),
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t("Define the color code you would like to use for the border and the text of the search box. If you don't know the color code click on the !link link !link_close for a color code picker.", array('!link' => '<a href="#" onclick="javascript:window.open(\'http://www.hispacar.com/affiliate/colorpicker/colorpicker.php?lang=en\', \'\', \'width=320, height=300\')">', '!link_close' => '</a>')),
    '#required' => FALSE,
  );

  $form['car_rental_module_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color (#)'),
    '#default_value' => variable_get('car_rental_module_background_color', 'FFFFFF'),
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t("Define the background color code of the searchbox. Use the !link color code picker !link_close if you don't know the color code.", array('!link' => '<a href="#" onclick="javascript:window.open(\'http://www.hispacar.com/affiliate/colorpicker/colorpicker.php?lang=en\', \'\', \'width=320, height=300\')">', '!link_close' => '</a>')),
    '#required' => FALSE,
  );

  $form['car_rental_module_button_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Button color (#)'),
    '#default_value' => variable_get('car_rental_module_button_color', 'FF0000'),
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t("Define the color of the call to action button. Use the !link color code picker !link_close if you don't know the color code.", array('!link' => '<a href="#" onclick="javascript:window.open(\'http://www.hispacar.com/affiliate/colorpicker/colorpicker.php?lang=en\', \'\', \'width=320, height=300\')">', '!link_close' => '</a>')),
    '#required' => FALSE,
  );

  $form['car_rental_module_button_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Button text color (#)'),
    '#default_value' => variable_get('car_rental_module_button_text_color', '000000'),
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t("Define the color of the text on the call to action button. Use the !link color code picker !link_close if you don't know the color code.", array('!link' => '<a href="#" onclick="javascript:window.open(\'http://www.hispacar.com/affiliate/colorpicker/colorpicker.php?lang=en\', \'\', \'width=320, height=300\')">', '!link_close' => '</a>')),
    '#required' => FALSE,
  );

  $form['car_rental_module_border_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Border width (pixels)'),
    '#default_value' => variable_get('car_rental_module_border_width', '0'),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Define the width of the search box border. The value is set to zero by default (no border)'),
    '#required' => FALSE,
  );

  $form['car_rental_module_border_radius'] = array(
    '#type' => 'textfield',
    '#title' => t('Border radius (pixels)'),
    '#default_value' => variable_get('car_rental_module_border_radius', '0'),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Define the radius of the search box border if you would prefer a rounded searchbox. The value is set to zero by default (square corners)'),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}


/**
 * Implements validation from the Form API.
 */
function car_rental_module_form_validate($form, &$form_state) {

  $car_rental_module_title = $form_state['values']['car_rental_module_title'];
  $the_match = preg_match('/^[a-z0-9 á-úÁ-Ú]{1,32}$/i', $car_rental_module_title);

  if ($car_rental_module_title && !$the_match) {
    form_set_error('car_rental_module_title', t('Title: please, use only letters, numbers and spaces.'));
  }

  $car_rental_module_af_cod = $form_state['values']['car_rental_module_af_cod'];
  $the_match = preg_match('/^[a-z0-9]{1,16}$/i', $car_rental_module_af_cod);

  if ($car_rental_module_af_cod && !$the_match) {
    form_set_error('car_rental_module_af_cod', t('Affiliate ID: please, use only letters and numbers.'));
  }

  $car_rental_module_lang = $form_state['values']['car_rental_module_lang'];
  $the_match = preg_match('/^[a-z]{2}$/i', $car_rental_module_lang);

  if (!$the_match) {
    form_set_error('car_rental_module_lang', t('Search box language: please, select one option.'));
  }

  $car_rental_module_default_loc_id = $form_state['values']['car_rental_module_default_loc_id'];
  $the_match = preg_match('/^[0-9]{1,16}$/i', $car_rental_module_default_loc_id);

  if ($car_rental_module_default_loc_id && !$the_match) {
    form_set_error('car_rental_module_default_loc_id', t('Location ID: please, use only numbers.'));
  }

  $car_rental_module_format = $form_state['values']['car_rental_module_format'];
  $the_match = preg_match('/^[a-z_]{1,16}$/i', $car_rental_module_format);

  if (!$the_match) {
    form_set_error('xxx', t('Format: please, select one option.'));
  }

  $car_rental_module_width = $form_state['values']['car_rental_module_width'];
  $the_match = preg_match('/^[0-9]{1,3}$/i', $car_rental_module_width);

  if (!$the_match) {
    form_set_error('car_rental_module_width', t('Width: please, use only numbers.'));
  }

  $car_rental_module_foreground_color = $form_state['values']['car_rental_module_foreground_color'];
  $the_match = preg_match('/^[a-f0-9]{6}$/i', $car_rental_module_foreground_color);

  if (!$the_match) {
    form_set_error('car_rental_module_foreground_color', t('Foreground color: please, enter six hexadecimal digits.'));
  }

  $car_rental_module_background_color = $form_state['values']['car_rental_module_background_color'];
  $the_match = preg_match('/^[a-f0-9]{6}$/i', $car_rental_module_background_color);

  if (!$the_match) {
    form_set_error('car_rental_module_background_color', t('Background color: please, enter six hexadecimal digits.'));
  }

  $car_rental_module_button_color = $form_state['values']['car_rental_module_button_color'];
  $the_match = preg_match('/^[a-f0-9]{6}$/i', $car_rental_module_button_color);

  if (!$the_match) {
    form_set_error('car_rental_module_button_color', t('Button color: please, enter six hexadecimal digits.'));
  }

  $car_rental_module_button_text_color = $form_state['values']['car_rental_module_button_text_color'];
  $the_match = preg_match('/^[a-f0-9]{6}$/i', $car_rental_module_button_text_color);

  if (!$the_match) {
    form_set_error('car_rental_module_button_text_color', t('Button text color: please, enter six hexadecimal digits.'));
  }

  $car_rental_module_border_width = $form_state['values']['car_rental_module_border_width'];
  $the_match = preg_match('/^[0-9]{1,3}$/i', $car_rental_module_border_width);

  if (!$the_match) {
    form_set_error('car_rental_module_border_width', t('Border width: please, use only letters and numbers.'));
  }

  $car_rental_module_border_radius = $form_state['values']['car_rental_module_border_radius'];
  $the_match = preg_match('/^[0-9]{1,3}$/i', $car_rental_module_border_radius);

  if (!$the_match) {
    form_set_error('car_rental_module_border_radius', t('Border radius: please, use only letters and numbers.'));
  }
}


/**
 * Renders the search form.
 */
function car_rental_module_render() {

  $plugin_url = dirname(__FILE__);
  $plugin_folder      = base_path() . drupal_get_path('module', 'car_rental_module');
  $title              = variable_get('car_rental_module_title', '');
  $af_cod             = variable_get('car_rental_module_af_cod', '');
  $lang               = variable_get('car_rental_module_lang', 'en');
  $pickup_id          = variable_get('car_rental_module_default_loc_id', '');
  $form_style         = variable_get('car_rental_module_format', 'c_c_c_half_width');
  $width              = variable_get('car_rental_module_width', '0');
  $color              = variable_get('car_rental_module_foreground_color', '000000');
  $background_color   = variable_get('car_rental_module_background_color', 'FFFFFF');
  $button_color       = variable_get('car_rental_module_button_color', 'FF000000');
  $button_text_color  = variable_get('car_rental_module_button_text_color', '000000');
  $border_width       = variable_get('car_rental_module_border_width', '0');
  $border_radius      = variable_get('car_rental_module_border_radius', '0');
  $def_pick_id = FALSE;
  $def_pick_name = FALSE;

  // Do we have a default location?
  if ($pickup_id) {
    // Autocomplete json load.
    $auto_json = file_get_contents($plugin_url . "/autocomplete_$lang.json");
    $left_pos = strpos($auto_json, '(') + 1;
    $right_pos = strrpos($auto_json, ')') - strpos($auto_json, '(') - 1;
    $auto_json = substr($auto_json, $left_pos, $right_pos);
    $auto_json = json_decode($auto_json);

    foreach ($auto_json as $json_entry) {
      if ($json_entry->value == $pickup_id) {
        $def_pick_id = $pickup_id;
        $def_pick_name = $json_entry->label;
      }
    }
  }

  $template = '';
  $template .= '<div id="c_h_h_cont" style="width:' . $width . '%">';
  $template .= '<div id="search_box" style="border-width:';
  $template .= $border_width . 'px;border-color:#' . $color;
  $template .= ';border-radius:';
  $template .= $border_radius . 'px;background-color:#' . $background_color;
  $template .= ';color:#' . $color . '">';
  $template .= '<form method="post" class="' . $form_style;
  $template .= '" action="http://www.hispacar.com/affiliate/redir.php" ';
  $template .= 'target="_blank"';
  $template .= 'onsubmit="javascript:return c_h_h_validate_form()">';
  $template .= '<h6>' . $title . '</h6>';
  $template .= '<div id="validation_errors">';
  $template .= '<span id="validation_errors_txt"></span>';
  $template .= '<a href="#" onclick="jQuery(\'#validation_errors\').hide();';
  $template .= 'return false" class="close">';
  $template .= 'X</a></div>';
  $template .= '<input id="pu_location" name="pu_location" class="loc" ';
  $template .= 'type="text" placeholder="' . car_rental_module_t('write_location', $lang);
  $template .= '" onclick="javascript:this.value=\'\'" value="';
  $template .= $def_pick_name . '"/>';
  $template .= '<input type="hidden" id="pu_location_id" name="pu_location_id"';
  $template .= 'value="' . $def_pick_id . '"/>';
  $template .= '<input type="hidden" id="c_h_h_lang" value="' . $lang;
  $template .= '" name="lang"/>';
  $template .= '<input type="hidden" id="af_cod" name="af_cod" value="';
  $template .= $af_cod . '"/>';
  $template .= '<div id="dif_ret_block"><input type="checkbox" ';
  $template .= 'id="dif_ret_switch" ';
  $template .= 'onclick="javascript:jQuery(\'#do_location\').toggle()"/> ';
  $template .= car_rental_module_t('return_other', $lang) . '</div>';
  $template .= '<input id="do_location" name="do_location" class="loc" ';
  $template .= 'placeholder="' . car_rental_module_t('write_location', $lang);
  $template .= '" onclick="javascript:this.value=\'\'" value=""/>';
  $template .= '<input type="hidden" id="do_location_id" ';
  $template .= 'name="do_location_id"/>';
  $template .= '<div class="sep1"></div>';
  $template .= '<input type="text" id="pu_date" name="pu_date" class="date" ';
  $template .= 'placeholder="' . car_rental_module_t('start_date', $lang) . '"/>';
  $template .= '<select id="pu_time" name="pu_time" class="time">';
  $template .= '<option value="00:00">00:00</option>';
  $template .= '<option value="00:30">00:30</option>';
  $template .= '<option value="01:00">01:00</option>';
  $template .= '<option value="01:30">01:30</option>';
  $template .= '<option value="02:00">02:00</option>';
  $template .= '<option value="02:30">02:30</option>';
  $template .= '<option value="03:00">03:00</option>';
  $template .= '<option value="03:30">03:30</option>';
  $template .= '<option value="04:00">04:00</option>';
  $template .= '<option value="04:30">04:30</option>';
  $template .= '<option value="05:00">05:00</option>';
  $template .= '<option value="05:30">05:30</option>';
  $template .= '<option value="06:00">06:00</option>';
  $template .= '<option value="06:30">06:30</option>';
  $template .= '<option value="07:00">07:00</option>';
  $template .= '<option value="07:30">07:30</option>';
  $template .= '<option value="08:00">08:00</option>';
  $template .= '<option value="08:30">08:30</option>';
  $template .= '<option value="09:00">09:00</option>';
  $template .= '<option value="09:30">09:30</option>';
  $template .= '<option value="10:00" selected="selected">10:00</option>';
  $template .= '<option value="10:30">10:30</option>';
  $template .= '<option value="11:00">11:00</option>';
  $template .= '<option value="11:30">11:30</option>';
  $template .= '<option value="12:00">12:00</option>';
  $template .= '<option value="12:30">12:30</option>';
  $template .= '<option value="13:00">13:00</option>';
  $template .= '<option value="13:30">13:30</option>';
  $template .= '<option value="14:00">14:00</option>';
  $template .= '<option value="14:30">14:30</option>';
  $template .= '<option value="15:00">15:00</option>';
  $template .= '<option value="15:30">15:30</option>';
  $template .= '<option value="16:00">16:00</option>';
  $template .= '<option value="16:30">16:30</option>';
  $template .= '<option value="17:00">17:00</option>';
  $template .= '<option value="17:30">17:30</option>';
  $template .= '<option value="18:00">18:00</option>';
  $template .= '<option value="18:30">18:30</option>';
  $template .= '<option value="19:00">19:00</option>';
  $template .= '<option value="19:30">19:30</option>';
  $template .= '<option value="20:00">20:00</option>';
  $template .= '<option value="20:30">20:30</option>';
  $template .= '<option value="21:00">21:00</option>';
  $template .= '<option value="21:30">21:30</option>';
  $template .= '<option value="22:00">22:00</option>';
  $template .= '<option value="22:30">22:30</option>';
  $template .= '<option value="23:00">23:00</option>';
  $template .= '<option value="23:30">23:30</option>';
  $template .= '</select>';
  $template .= '<div class="sep2"></div>';
  $template .= '<input type="text" id="do_date" name="do_date" class="date" ';
  $template .= 'placeholder="' . car_rental_module_t('end_date', $lang) . '"/>';
  $template .= '<select id="do_time" name="do_time" class="time">';
  $template .= '<option value="00:00">00:00</option>';
  $template .= '<option value="00:30">00:30</option>';
  $template .= '<option value="01:00">01:00</option>';
  $template .= '<option value="01:30">01:30</option>';
  $template .= '<option value="02:00">02:00</option>';
  $template .= '<option value="02:30">02:30</option>';
  $template .= '<option value="03:00">03:00</option>';
  $template .= '<option value="03:30">03:30</option>';
  $template .= '<option value="04:00">04:00</option>';
  $template .= '<option value="04:30">04:30</option>';
  $template .= '<option value="05:00">05:00</option>';
  $template .= '<option value="05:30">05:30</option>';
  $template .= '<option value="06:00">06:00</option>';
  $template .= '<option value="06:30">06:30</option>';
  $template .= '<option value="07:00">07:00</option>';
  $template .= '<option value="07:30">07:30</option>';
  $template .= '<option value="08:00">08:00</option>';
  $template .= '<option value="08:30">08:30</option>';
  $template .= '<option value="09:00">09:00</option>';
  $template .= '<option value="09:30">09:30</option>';
  $template .= '<option value="10:00" selected="selected">10:00</option>';
  $template .= '<option value="10:30">10:30</option>';
  $template .= '<option value="11:00">11:00</option>';
  $template .= '<option value="11:30">11:30</option>';
  $template .= '<option value="12:00">12:00</option>';
  $template .= '<option value="12:30">12:30</option>';
  $template .= '<option value="13:00">13:00</option>';
  $template .= '<option value="13:30">13:30</option>';
  $template .= '<option value="14:00">14:00</option>';
  $template .= '<option value="14:30">14:30</option>';
  $template .= '<option value="15:00">15:00</option>';
  $template .= '<option value="15:30">15:30</option>';
  $template .= '<option value="16:00">16:00</option>';
  $template .= '<option value="16:30">16:30</option>';
  $template .= '<option value="17:00">17:00</option>';
  $template .= '<option value="17:30">17:30</option>';
  $template .= '<option value="18:00">18:00</option>';
  $template .= '<option value="18:30">18:30</option>';
  $template .= '<option value="19:00">19:00</option>';
  $template .= '<option value="19:30">19:30</option>';
  $template .= '<option value="20:00">20:00</option>';
  $template .= '<option value="20:30">20:30</option>';
  $template .= '<option value="21:00">21:00</option>';
  $template .= '<option value="21:30">21:30</option>';
  $template .= '<option value="22:00">22:00</option>';
  $template .= '<option value="22:30">22:30</option>';
  $template .= '<option value="23:00">23:00</option>';
  $template .= '<option value="23:30">23:30</option>';
  $template .= '</select>';
  $template .= '<div class="sep3"></div>';
  $template .= '<input type="submit" id="doSubmit" value="';
  $template .= car_rental_module_t('search_cars', $lang);
  $template .= '" class="butt" style="background-color:#';
  $template .= $button_color . ';border-color:#';
  $template .= $button_color . ';color:#';
  $template .= $button_text_color . '"/>';
  $template .= '</form>';
  $template .= '</div>';
  $template .= '<div class="' . $form_style . '">';
  $template .= '<div class="powered_hispacar">powered by: ';
  $template .= '<img src="';
  $template .= $plugin_folder . '/hispacar_logo.png" alt="Hispacar"/>';
  $template .= '</div></div></div>';

  $template .= "\n<script type=\"text/javascript\">";
  $template .= "\nvar crbeSettings = {";
  $template .= "\nlang: '" . $lang . "',";
  $template .= "\nplugin_url: '" . $plugin_folder . "',";
  $template .= "\nerror_txt_review_highlighted: '";
  $template .= car_rental_module_t("error_txt_review_highlighted", $lang) . "',";
  $template .= "\nerror_txt_type_city: '";
  $template .= car_rental_module_t("error_txt_type_city", $lang) . "',";
  $template .= "\nerror_txt_select_start_date: '";
  $template .= car_rental_module_t("error_txt_select_start_date", $lang) . "',";
  $template .= "\nerror_txt_select_end_date: '";
  $template .= car_rental_module_t("error_txt_select_end_date", $lang) . "'";
  $template .= "\n}";
  $template .= "\ninitVars(crbeSettings);";
  $template .= "\n</script>";

  return "$template";
}

/**
 * Translates search form labels.
 */
function car_rental_module_t($label_key, $lang) {

  static $labels;

  if (!is_array($labels)) {
    $raw_text = "
      write_location|Escriba ciudad|Entrer la ville|Stadt|Type in city|Nome della Città|Vul plaats in
      start_date|Fecha de inicio|Date de début|Mietbeginn|Start date|Data di inizio|Begindatum
      end_date|Fecha de fin|Date de fin|Mietende|End date|Data di fine|Einddatum
      return_other|Devolver en un punto diferente|Lieu de retour différent|Rückgabe an einem anderen Ort|Return to a different location|Riconsegna in altro luogo|Op andere locatie inleveren
      search_cars|Buscar coches|Chercher une voiture|Fahrzeug suchen|Search for cars|Cercare un auto|Auto's zoeken
      error_txt_review_highlighted|Revise los campos resaltados|Veuillez modifier les champs en surbrillance|Bitte markierte Felder prüfen|Please review the highlighted fields|Controlla i campi evidenziati|Controleer de geaccentueerde velden
      error_txt_type_city|Escriba ciudad|Veuillez saisir une ville|Bitte geben Sie eine Stadt|Please type in a city|Digita il nome di una città|Typ hier de naam van een plaats
      error_txt_select_start_date|Seleccione una fecha de inicio|Veuillez sélectionner une date de début|Bitte wählen Sie ein Anfangsdatum|Please select a start date|Seleziona una data di inizio noleggio|Kies een begindatum
      error_txt_select_end_date|Seleccione una fecha de fin|Veuillez sélectionner une date de fin|Bitte wählen Sie ein Enddatum|Please select an end date|Seleziona una data di fine noleggio|Kies een einddatum";

    $lines = explode("\n", $raw_text);
    $labels = array();

    foreach ($lines as $line) {
      $line = trim($line);
      if (!$line) {
        continue;
      }
      $parts = explode("|", $line);
      $label = array();
      $key = $parts[0];
      $label['es'] = $parts[1];
      $label['fr'] = $parts[2];
      $label['de'] = $parts[3];
      $label['en'] = $parts[4];
      $label['it'] = $parts[5];
      $label['nl'] = $parts[6];
      $labels[$key] = $label;

    }

  }

  return $labels[$label_key][$lang];

}
