CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Troubleshooting
* FAQ
* Maintainers


INTRODUCTION
------------
This Drupal module adds a car hire search box to a Joomla website so visitors 
can compare prices and book online with over 750 car hire companies worldwide. 


REQUIREMENTS
------------
There are no special requirements to install and use this module.


INSTALLATION
------------
* Install as you would normally install a contributed drupal module. See 
https://drupal.org/documentation/install/modules-themes/modules-7 for further 
information.


CONFIGURATION
-------------
 After installing the module, use the setup options to customize your search.

* Title: introduce an appropriate title.

* Affiliate ID: If you want to monetize the plugin you need to request an 
affiliate ID. Click on the link to register and request an affiliate ID. 
The searchbox will work fine for non-registered sites as well, however to get 
paid you must use an affiliate ID otherwise the plugin will be unable to track 
any commissions.

* Search box language: Select the language you would like to use on the search 
box. If you do not select a language English will be used by default.

* Default location: This field can be set to a default location if you prefer. 
Visitors can still search for a car in any destination worldwide by typing in 
a different destination.

* Text and Border color: Define the color code you would like to use for the 
border and the text of the search box. If you don't know the color code click 
on the link for a color code picker.

* Background color: Define the background color code of the searchbox. Use 
the color code picker if you don't know the color code.

* Button color: Define the color of the call to action button. Use the 
color code picker if you don't know the color code.

* Button text color: Define the color of the text on the call to action 
button. Use the color code picker if you don't know the color code.

* Border width: Define the width of the search box border. The value is set to 
zero by default (no border).

* Border radius: Define the radius of the search box border if you would 
prefer a rounded searchbox. The value is set to zero by default (square 
corners).


FAQ
---
* Do I need to register in order to use the plugin?
You can use the module without registration, but if you want to monetize it 
you need to request an affiliate ID.

* How can I monetize the module on my travel site?
If you want to monetize the module you need to request an affiliate ID. The 
searchbox will work fine for non-registered sites as well, however to get paid 
you must use an affiliate ID otherwise the module will be unable to track any 
commissions.

* How can I get an Affiliate ID?
You can  request an affiliate ID on the official homepage of the module.


MAINTAINERS
-----------
Current maintainers:
* Hispacar (Spain) - https://www.drupal.org/u/hispacar
