﻿Car Rental Module for Drupal

This Module adds a car rental search box to a Drupal site so visitors can 
check availability and compare prices of over 750 car hire companies 
worldwide.
This Module is specifically developed by Hispacar for travel related websites 
running on Joomla. It will automatically calculate prices and availability in 
more than 30.000 destinations around the world. 
It is a great source of income for travel sites as every booking automatically 
generates a commission.

Main Features

Show car hire prices in more than 30.000 destinations worldwide check 
availability in more than 150 different countries list vehicles from over 750 
suppliers worldwide and growing earn commissions for every booking made on 
your website


Installation

This is the latest stable build of the car rental module for Drupal.

Install as you would normally install a contributed drupal module. See 
https://drupal.org/documentation/install/modules-themes/modules-7 for further 
information. If you would like to contribute to or test the developer build 
please contact our development team.</p>


Support

For support you can contact us directly at the module's official homepage.


Bugs

The module has been tested up to Drupal version 7. If you find an issue, let 
us know!


Frequently Asked Questions

How can I get an Affiliate ID?
Visit http://www.hispacar.com/drupal-modules/search/en/register/ and request 
an affiliate ID on the website of Hispacar.

Do I need to register in order to use the module?
You can use the module without registration, but if you want to monetize the 
module you need to request an affiliate ID.


Module's official Homepage

More information about the module and a detailled step-by-step installation 
and usage guide can be found on the module's official homepage.
